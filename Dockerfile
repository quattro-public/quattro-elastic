FROM docker.elastic.co/elasticsearch/elasticsearch:6.6.1

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install -b https://distfiles.compuscene.net/elasticsearch/elasticsearch-prometheus-exporter-6.6.1.0.zip
